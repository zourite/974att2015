</div>
<footer>

    <section class="row">

    <nav id="navigation-foot" >

        <div class="small-6 columns">
            <h4 class="wht-txt">Informations</h4>
            <?php

                wp_nav_menu(

                    array(
                    'menu' => 'footer-menu',
                    'menu_class' => 'menu vertical gr-txt',
                    'menu_id' => 'footer-menu',

                    )
                );

            ?>
        </div>

    </nav>

        <div class="small-6 columns">
            
            <p class="mrg-top wht-txt">Inscris toi et bénéficie de 5% de remise sur ta prochaine commande.</p>

            <div id="mc_embed_signup">
                
                <form action="//974attitude.us15.list-manage.com/subscribe/post?u=fcf22c02d89eef61b65c2a336&amp;id=c6ae8bff5b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                
                    <div id="mc_embed_signup_scroll">
    
                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Ton adresse e-mail" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_fcf22c02d89eef61b65c2a336_c6ae8bff5b" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Je veux la remise" name="subscribe" id="mc-embedded-subscribe" class="button button-highlight"></div>
                    </div>
                
                </form>
            
            </div>
            
            <!--End mc_embed_signup-->

        </div>

    <div class="row">

        <div class="small-12 columns">

            <p class="wht-txt text-center mrg-top">

                <small>Boire un petit coup c'est agréable... L'abus d'alcool est dangereux pour la santé. A consommer avec modération. Manger ! Bouger !</small>

            </p>

        </div>

    </div>
    
    </section>

</footer>

    </div>

    </div>

</div>

<?php wp_footer(); ?>

<script src="<?php bloginfo('stylesheet_directory'); ?>/js/foundation.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>

</body>
</html>

