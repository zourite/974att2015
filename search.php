<?php get_header() ?>

<section class="row box-cont">

    <div class="small-12 columns">

        <h1 class="orange spacing">Résultats de ta recherche :</em></h1>

        <?php

        $search_query = get_search_query();
        $product_search = new WP_Query("s=$search_query&post_type=product");
        $recette_search = new WP_Query("s=$search_query&post_type=post&category_name=recette");
        $post_search = new WP_Query("s=$search_query&post_type=post&cat=-10");
        wp_reset_query();

        $cat_search = array(

            'produit' => array(
                'query' => $product_search,
                'url' => 'post_type=product'
                ),
            "recette" => array(
                'query'=> $recette_search,
                'url' =>  'post_type=post&category_name=recette'
                ),
            "article" => array(

                'query' => $post_search,
                'url' => 'post_type=post&cat=-10'

                ));

        ?>

        <p>

            <?php

                foreach ($cat_search as $key => $cat) :

                    if($cat['query']->found_posts > 0): $url = $cat['url']?>


                     <a href="<?php bloginfo('url'); echo "/?s=$search_query&$url"?>" class="button button-highlight">

                        <?php

                        echo $cat['query']->found_posts.'&nbsp;'; echo $key; echo ($cat['query']->found_posts>1)?'s':'';

                            if ($key == "produits"):

                                echo '&nbsp;à vendre';

                            endif;

                        ?>

                    </a>
            <?php

                    endif;

                endforeach;

            ?>

        </p>

        <div class="row small-up-1 medium-up-2" data-equalizer data-equalize-by-row="true">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="column" >

                <article class="card" data-equalizer-watch itemtype="http://schema.org/Article">

                    <div class="ribbon <?php echo ribbon() ?>"></div>

                    <a href="<?php echo the_permalink(); ?>">

                     <?php

                            if (has_post_thumbnail($post->ID)) :

                                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'homepage-thumb' ); ?>

                    <img itemprop="image" class="thumb-box" src="<?php echo $image[0]; ?>" alt="<?php echo the_title() ?>">

                    <?php else : ?>

                    <img itemprop="image" class="thumb-box" src="<?php bloginfo('stylesheet_directory'); ?>/img/banniere_974attitude.png" alt="<?php echo the_title() ?>">

                    <?php endif; ?>

                    </a>

                    <div class="pad">

                    <h3 itemprop="name"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>

                    <p class="post-desc" itemprop="articleBody">

                        <?php echo get_the_excerpt(); ?>

                        <span class="hashtag">

                            <small>

                            <?php if(get_the_tags()) : $tags = get_the_tags(); foreach($tags as $tag) : ?>

                            <a href="<?php echo get_tag_link($tag->term_id);?>">#<?php echo $tag->name ?></a>

                            <?php  endforeach; endif;?>

                            </small>

                        </span>

                    </p>

                    <meta itemprop="datePublished" content="<?php the_time('c'); ?>"/>

                    </div>

                </article>

                <div class="card-meta meta-list text-right">

                    <span class="calendar-i">

                       <small> <i class="fa fa-calendar"></i> <?php the_time('d F Y'); ?> </small>

                    </span>|

                    <span class="read-i">

                       <small> <i class="fa fa-clock-o"></i>Lecture de <?php echo do_shortcode('[rt_reading_time postfix="minutes"]') ?></small>

                    </span>

                </div>

            </div>

        <?php endwhile; endif; ?>

        </div>

    </div>

</section>

<section class="row">

    <nav class="woocommerce-pagination small-12 card columns menu-centered">



        <?php  echo att2015_paging_nav(); ?>

    </nav>

</section>

<?php get_footer(); ?>
