module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({

        concat: {

            css: {
                //Concatenate all of the files in the cssResources configuration property
                src: ['css/header.min.css','css/footer.min.css','css/product.min.css','css/index.min.css','css/main.min.css' ],
                dest: 'style.css'
            }

        },

        watch: {

            js: {
                files: ['js/*.js'],
                options: {
                    // livereload: true,
                    spawn: false
                }
            },
            css: {
                files: ['css/*.css'],
                tasks:['default'],
                options: {
                    // livereload: true,
                    //event:['all'],
                    spawn: false
                }
            },
            scss: {
                files: ['scss/*.scss'],
                tasks:['compass'],
                options: {
                    // livereload: true,
                    //event:['all'],
                    spawn: false
                }
            },
            php: {
                files: ['*.php','woocommerce/*.php'],
                //tasks:['php'],
                options: {
                    // livereload: true,
                    spawn: false
                }
            }
        },

        cssmin : {
			minify : {
				expand : true,
				cwd : './css/',
				src : [ '*.css', '!*.min.css' ],
				dest : './css/',
				ext : '.min.css'
			}
		},

        browserSync: {
            dev: {

                bsFiles: {
                    src : ['*.css', '*.php', 'woocommerce/*.php', 'woocommerce/**/*.php']
                },

                options: {

                    watchTask: true,
                    proxy: "romeo.dev/974attitude",

                }
            }
        },

        compass: {

            dist: {

                options: {

                    sassDir: 'scss',
                    cssDir: 'css',
                    environment: 'production',
                }
            }
        }


    });

	grunt.registerTask( 'default', ['compass','cssmin','concat' ] );
	grunt.registerTask('sync', ['browserSync','watch']);

}
