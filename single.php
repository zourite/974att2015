<?php get_header(); ?>
<section class="row box-cont">

    <div class="small-12 medium-12 large-12 columns">

        <article id="content" class="box single row column" itemscope itemtype="http://schema.org/Article">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h1 itemprop="headline"><?php the_title(); ?></h1>

            <div class="meta-list">

                <span class="calendar-i">

                    <i class="fa fa-calendar"></i> <?php the_time('d F Y'); ?>

                </span> |

                <span class="read-i">

                    <span class="yellow"><i class="fa fa-clock-o"></i></span>Lecture de <?php echo do_shortcode('[rt_reading_time postfix="minutes"]') ?>

                </span>

            </div>

            <?php if (has_post_thumbnail($post->ID)) :

                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'single-thumb');

                ?>

                <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject" id="thumb-single">

                    <img class="thumb margin-bot raised" src="<?php echo $image[0]; ?>" alt="<?php echo the_title() ?>">
                    <meta itemprop="url" content="<?php echo $image[0]; ?>">

                </div>

            <?php endif; ?>

            <div itemprop="articleBody">

            <?php the_content(); ?>

            </div>

            <div>
<h4>Inscris toi pour rester informé</h4>
                <!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup" class="small-7 small-centered">
<form action="//974attitude.us15.list-manage.com/subscribe/post?u=fcf22c02d89eef61b65c2a336&amp;id=c6ae8bff5b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll" class="row">
    
    <input class="small-7 columns" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_fcf22c02d89eef61b65c2a336_c6ae8bff5b" tabindex="-1" value=""></div>
    <div class="clear small-5 columns"><input type="submit" value="Je m'inscris" name="subscribe" id="mc-embedded-subscribe" class="button button-highlight"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->

    </div>

            <meta itemprop="datePublished" content="<?php the_time('c'); ?>"/>

            <div class="meta-list">

                <?php if(get_the_tags()) : ?>
                <span class="tags-i">
                    <i class="fa fa-tag"></i> <?php the_tags(); ?>
                </span> |
                <?php endif; ?>

                <?php if(get_the_category()) :?>
                <span class="category-i">
                    <i class="fa fa-tags"></i> <?php the_category(' - '); ?>
                </span>
                <?php endif; ?>

            </div>

            <?php endwhile; endif; ?>

        </article>

    </div>

</section>
<?php

    $rand_products = get_related_product_for_single();

    if(!empty($rand_products)) :?>

<section class="row box-cont">

    <div class="small-12 medium-12 large-12 columns">

        <h3>Ces produits peuvent t'intéresser</h3>

         <div class="row small-up-1 medium-up-4" data-equalizer data-equalize-by-row="true">

    <?php

    foreach($rand_products as $product ) :

        $post_id = $product->ID;
        $price = get_post_meta($post_id, '_price', true);
        $thumb_url = get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'thumbs-home' ) );

    ?>



        <div class="column">

            <div class="card" data-equalizer-watch>

                <div class="clean-height">

                <?php echo $thumb_url; ?>

                </div>

                <div class="text-center pad">

                    <a href="<?php echo get_permalink ($post_id); ?>">

                        <span class="black"><?php echo $product->post_title; ?></span>
                        <span class="price-card"> <?php echo money_format('%i', (float) $price); ?> €</span>

                    </a>

                </div>

            </div>

        </div>

    <?php

    endforeach;

    wp_reset_query();

    ?>

    </div>

    <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="float-right fa-but button button-highlight">Voir tous les produits de la boutique <i class="fa fa-check" aria-hidden="true"></i></a>

    </div>

</section>

<?php endif; ?>

<section class="row box-cont">

    <div class="small-12 medium-12 large-12 columns">

        <?php comments_template(); ?>

    </div>

</section>

<?php get_footer(); ?>
