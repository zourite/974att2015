<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

<section class="row box-cont">


 <div class="small-12 columns">
    <?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
    ?>

   <?php woocommerce_template_single_title(); ?>

</div>



<div class="small-12 medium-6 columns">
	<?php

        $product = wc_get_product(); ?>

    <div id="product_image">

        <?php

		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );

            //echo $product->get_image('single');
	   ?>

    </div>

</div>

	<div class="small-12 medium-6 columns summary entry-summary">

        <div class="box">

		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			//do_action( 'woocommerce_single_product_summary' );

            //woocommerce_template_single_title(); ?>

        <div class="margin-bot">

            <span class="yellow"><?php get_rate_star($product); ?></span> -
            Il y à <?php comments_number( 'aucun avis', 'un avis', '% avis' ); ?> pour ce produit

        </div>

        <div id="price" class="margin-bot text-center">

            <span class="red price-p"><?php echo $product->get_price(); ?> €</span> 

          <!--  - <span>

                <?php
                    
                    /*

                    if (substr($product->get_weight(), 0, 1) == 0) :

                        echo ($product->get_weight()*1000).'gr';

                    else :

                        echo $product->get_weight().'kg';

                    endif;
                    
                    */

                ?>

            </span> -->

        </div>

        <div id="add_to_cart">

            <?php woocommerce_template_single_add_to_cart(); ?>

        </div>

        <div id="compo_product">

            <div id="excerpt" class="margin-bot mrg-top">

            <?php echo nl2br(get_the_excerpt()); ?>
             <img class="float-center mrg-top margin-bot" src="<?php bloginfo('stylesheet_directory'); ?>/img/visa-paypal-mastercard.jpg" alt="Visa, Paypal, Mastercard">
            </div>

            <?php if(get_products_tag($product->id)) : ?>

            <div class="margin-bot">

                <small>

                    <i class="fa fa-tags"></i>

                    <?php

                        $category = get_products_category($product->id);

                        foreach (get_products_tag($product->id) as $tags) : ?>


                            <a href="<?php echo get_tag_link($tags[0]); ?>">#<?php echo $tags[1]; ?> </a>

                    <?php

                        endforeach;

                    ?>

                        <a href="<?php echo get_tag_link($category->term_id); ?>">#<?php echo $category->name; ?> </a>

                </small>

            </div>

        <?php endif; ?>

        </div>

	</div><!-- .summary -->

</section>

<section class="row box-cont">
	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

<div class="small-12 column">

        <?php the_content(); ?>

</div>

</section>

</div>

<section id="related-product" class="row box-cont">

    <div class="small-12 column">

        <h2>Les produits similaires à <?php woocommerce_template_single_title(); ?><small>(<em>ou pas</em>)</small></h2>

        <div class="row small-up-1 medium-up-5" data-equalizer data-equalize-by-row="true">

    <?php

        $related_products = $product->get_related();

        foreach ($related_products as $other_product) :

            $other =  wc_get_product($other_product);

            $post_id = $other->id;
            $price = get_post_meta($post_id, '_price', true);
            $thumb_url = get_the_post_thumbnail($post_id, 'thumbnail', array( 'class' => 'thumbs-home' ) ); ?>

             <div class="column">

            <div class="card" data-equalizer-watch>

               <div class="clean-height">
                <?php

                echo $thumb_url;

                ?>
            </div>

                <div class="text-center pad">

                    <a href="<?php echo get_permalink ($post_id); ?>">

                        <span class="black"><?php echo $other->post->post_title; ?></span>
                        <span class="price-card"> <?php echo money_format('%i', (float) $price); ?> €</span>

                    </a>

                </div>

            </div>

        </div>


    <?php

        endforeach;


    ?>

</div>

    </div>

</section>

    <section class="row box-cont">

        <div class="small-12 column">

            <?php comments_template(); ?>

        </div>

    </section>

<!-- </div> #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>


