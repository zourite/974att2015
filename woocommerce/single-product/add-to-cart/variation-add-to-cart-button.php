<?php
/**
 * Single variation cart button
 *
 * @see 	http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button input-group">
	<?php woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) ); ?>

    <div class="input-group-button">

        <input type="submit" class="button button-highlight" value="<?php echo esc_html( $product->single_add_to_cart_text() ); ?> ">

	   <!-- <button type="submit" class="single_add_to_cart_button button button-highlight alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button> -->

    </div>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->id ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->id ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
