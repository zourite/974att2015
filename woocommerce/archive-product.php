<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

 get_header( 'shop' ); ?>

<section class="row box-cont">

    <div class="small-12 medium-12 large-12 columns">

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action( 'woocommerce_before_main_content' );
	?>

	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

        <?php if(!is_shop()): ?>

            <h1 class="page-title"> <?php echo ucfirst(woocommerce_page_title(false)); ?></h1>

        <?php endif; ?>

    <?php endif; ?>

    <?php if(!is_shop()) : ?>

        <div class="page-desc">

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>

        </div>

    <?php endif; ?>

        <div id="tag-list" class="margin-bot">

        <div class="margin-bot">

            <strong>Filtrer : <br></strong>

        </div>

        <?php

        $listcats = get_sub_cat();

        $listags = get_tags_in_cat();

        if(is_shop()) { $listags = array(); $listcats = get_woo_search_cat();}

        $opt_filtre = array_merge($listcats,$listags);

        foreach($opt_filtre as $tags):

        ?>

        <a href="<?php echo $tags['link'] ?>" class="button button-highlight margin-bot">#<?php echo ucfirst($tags['name']) ?></a>

        <?php endforeach ?>

        </div>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				//do_action( 'woocommerce_before_shop_loop' );
			?>
            <div class="clearfix">

                <div class="float-right">

                    <?php woocommerce_catalog_ordering(); ?>

                </div>

            </div>

		<?php

        // woocommerce_product_subcategories($defaults);


        ?>

            <div class="row small-up-1 medium-up-4" data-equalizer data-equalize-by-row="true">

                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="column">

                        <?php wc_get_template_part( 'content', 'product' ); ?>

                    </div>

				<?php endwhile; // end of the loop. ?>

            </div>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

</div>
</section>

<?php get_footer( 'shop' ); ?>
