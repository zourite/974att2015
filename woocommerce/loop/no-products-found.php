<?php
/**
 * Displayed when no products are found matching the current query
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/no-products-found.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<p class="woocommerce-info text-center margin-bot"><?php _e( 'No products were found matching your selection.', 'woocommerce' ); ?></p>

 <?php

    $args     = array( 'post_type' => 'product', 'order_by' => 'rand' , 'posts_per_page'   => 8);
    $rand_products = get_posts($args);

    if(!empty($rand_products)) :?>

    <div class="small-12 columns">

    <div class="row small-up-1 medium-up-4" data-equalizer data-equalize-by-row="true">

    <?php

    foreach($rand_products as $product ) :

        $post_id = $product->ID;
        $price = get_post_meta($post_id, '_price', true);
        $thumb_url = get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'thumbs-home' ) );

    ?>



        <div class="column">

            <div class="card" data-equalizer-watch>
                <div class="clean-height">
                <?php echo $thumb_url; ?>
                </div>
                <div class="text-center pad">

                    <a href="<?php echo get_permalink ($post_id); ?>">

                        <span class="black"><?php echo $product->post_title; ?></span>
                        <span class="price-card"> <?php echo money_format('%i', (float) $price); ?> €</span>

                    </a>

                </div>

            </div>

        </div>

    <?php

    endforeach;

    wp_reset_query();

    ?>

    </div>

    <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="float-right fa-but button button-highlight">Voir tous les produits de la boutique <i class="fa fa-check" aria-hidden="true"></i></a>

    </div>

   <?php endif; ?>
