<?php get_header() ?>

<section class="row box-cont">

     <?php

    $args     = array( 'post_type' => 'product', 'orderby' => 'rand' , 'posts_per_page'   => 8);
    $rand_products = get_posts($args);

    if(!empty($rand_products)) :?>



    <div class="small-12 columns">

    <h2> Produits réunionnais et d'ailleurs...</h2>

    <div class="row small-up-1 medium-up-4" data-equalizer data-equalize-by-row="true">

    <?php

    foreach($rand_products as $product ) :

        $post_id = $product->ID;
        $price = get_post_meta($post_id, '_price', true);
        $thumb_url = get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'thumbs-home' ) );

    ?>



        <div class="column">

            <div class="card" data-equalizer-watch>

                <div class="clean-height">

                <?php echo $thumb_url; ?>

                </div>

                <div class="text-center pad">

                    <a href="<?php echo get_permalink ($post_id); ?>">

                        <span class="black"><?php echo $product->post_title; ?></span>
                        <span class="price-card"> <?php echo money_format('%i', (float) $price); ?> €</span>

                    </a>

                </div>

            </div>

        </div>

    <?php

    endforeach;

    wp_reset_query();

    ?>

    </div>

    <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="float-right fa-but button button-highlight">Voir tous les produits de la boutique <i class="fa fa-check" aria-hidden="true"></i></a>

    </div>

    <?php endif; ?>
</section>

<section class="row box-cont mrg-top pad">

    <div class="small-12 columns">

        <p>L'artisanat à la réunion est le fruit d’un savoir-faire particulier, l'artisan assure en général tous les stades de sa production ou de sa transformation. 974 Attitude est <strong>une boutique ou épicerie reunnionnaise (Boutique Chinois)</strong> en ligne et je te propose <strong>des produits réunionnais artisanaux</strong> (ou non) directement vendu en ligne et livré chez toi, ou chez une amie ou chez ta grand mère si tu le souhaites.</p>

        <p>Sur ce site tu pourras <strong>acheter des gelées, des vinaigres aromatisés, des lentilles de Cilaos, des pâtes de piment (Piment la pâte), des confitures</strong> et tout plein d’autre produits que j’ai la flemme d’écrire. Je te propose de <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ) ?>" title="Boutique en ligne" target="_blank">visiter la boutique</a> pour te faire un avis, sur les produits que je vends.</p>

        <p>Si tu désires <strong>avoir un produit en particulier,</strong> je te propose de <a href="<?php echo bloginfo('url')?>/contact">me contacter et de me faire ta demande</a>. J'essaierai de te combler, je tiens à préciser que je <strong>ne vends pas moi même des produits surgelés tel que les bouchons ou les samoussas.</strong> Cependant tu peux en achetter et te faire livrer chez toi directement par mon partenaire. Je te propose <a href="http://www.onclesam.com/index.php?eko=2BJIK8Z2NI" target="_blank">le pack découverte de samoussas ou de bouchon, surgelés.</a></p>

        <p>J’ai pris la décision de ne pas vendre de l’alcool sur mon site. Si tu veux savoir le pourquoi du comment, je te propose de lire l’article que j’ai écrit, pour <a href="http://974attitude.fr/acheter-alcool-reunion-metropole/" target="_blank">acheter de l’alcool réunionnais</a> à bas coûts et éviter les taxes douanières.</p>

        <p>Si tu te situe en métropole <em>(Lyon, Paris, Marseille et toutes les autres villes que possède La France)</em>, tu es au bon endroit pour commander des <strong>produits rares (ou pas) de la réunion.</strong></p>

    </div>

</section>

<section id="info-pratique" class="row box-cont pad">

    <div class="small-12  medium-6 columns hr-vert">

        <img class="float-center margin-bot" src="<?php bloginfo('stylesheet_directory'); ?>/img/chronopost_logo.png" alt="">

        <div class="text-center">

            <a href="<?php echo get_permalink(get_page_by_path('chronopost')); ?>">Une astuce pour diminuer les frais de port <i class="fa fa-info-circle" aria-hidden="true"></i></a>

        </div>

    </div>

    <div class="small-12 medium-6 columns">

        <img class="float-center margin-bot" src="<?php bloginfo('stylesheet_directory'); ?>/img/paypal-logo.png" alt="">

        <div class="text-center">

            <a href="<?php echo get_permalink(get_page_by_path('paypal')); ?>">Un remboursement simple et assuré <i class="fa fa-info-circle" aria-hidden="true"></i></a>

        </div>

    </div>

</section>

<section class="row box-cont">

    <div class="small-12 columns">

        <h2>Un bout de l'île de La Réunion</h2>

        <div class="row small-up-1 medium-up-2" data-equalizer data-equalize-by-row="true">

            <?php query_posts( 'posts_per_page=4' ); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="column" >

                <article class="card" data-equalizer-watch itemtype="http://schema.org/Article">

                    <div class="ribbon <?php echo ribbon() ?>"></div>

                    <a href="<?php echo the_permalink(); ?>">

                     <?php

                            if (has_post_thumbnail($post->ID)) :

                                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'homepage-thumb' ); ?>

                    <img itemprop="image" class="thumb-box" src="<?php echo $image[0]; ?>" alt="<?php echo the_title() ?>">

                    <?php else : ?>

                    <img itemprop="image" class="thumb-box" src="<?php bloginfo('stylesheet_directory'); ?>/img/banniere_974attitude.png" alt="<?php echo the_title() ?>">

                    <?php endif; ?>

                    </a>

                    <div class="pad">

                    <h3 itemprop="name"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>

                    <p class="post-desc" itemprop="articleBody">

                        <?php echo get_the_excerpt(); ?>

                        <span class="hashtag">

                            <small>

                            <?php if(get_the_tags()) : $tags = get_the_tags(); foreach($tags as $tag) : ?>

                            <a href="<?php echo get_tag_link($tag->term_id);?>">#<?php echo $tag->name ?></a>

                            <?php  endforeach; endif;?>

                            </small>

                        </span>

                    </p>

                    <meta itemprop="datePublished" content="<?php the_time('c'); ?>"/>

                    </div>



                </article>

                <div class="card-meta meta-list text-right">

                    <span class="calendar-i">

                       <small> <i class="fa fa-calendar"></i> <?php the_time('d F Y'); ?> </small>

                    </span>|

                    <span class="read-i">

                       <small> <i class="fa fa-clock-o"></i>Lecture de <?php echo do_shortcode('[rt_reading_time postfix="minutes"]') ?></small>

                    </span>

                </div>

            </div>

        <?php endwhile; endif; ?>

        </div>

    </div>

</section>

<!-- <section class="row">

    <div class="small-12 card columns">

        <?php // echo att2015_paging_nav(); ?>

    </div>

</section> -->

<?php get_footer(); ?>
