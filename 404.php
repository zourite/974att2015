<?php get_header() ?>
<section class="row box-cont">

    <div class="small-6 columns">

        <img class="float-center" src="<?php bloginfo('stylesheet_directory'); ?>/img/dodo-reunion.jpg" alt="">

    </div>

    <div class="small-6 columns">

        <p>Tu as trouvé le dodo, tu t'es donc perdu...</p>

    <?php

    $args     = array( 'post_type' => 'product', 'order_by' => 'rand' , 'posts_per_page'   => 6);
    $rand_products = get_posts($args);

    if(!empty($rand_products)) :?>

    <div class="row small-up-1 medium-up-3" data-equalizer data-equalize-by-row="true">

    <?php

    foreach($rand_products as $product ) :

        $post_id = $product->ID;
        $price = get_post_meta($post_id, '_price', true);
        $thumb_url = get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'thumbs-home' ) );

    ?>



        <div class="column">

            <div class="card" data-equalizer-watch>

                <?php echo $thumb_url; ?>

                <div class="text-center pad">

                    <a href="<?php echo get_permalink ($post_id); ?>">

                        <span class="black"><?php echo $product->post_title; ?></span>
                        <span class="price-card"> <?php echo money_format('%i', (float) $price); ?> €</span>

                    </a>

                </div>

            </div>

        </div>

    <?php

    endforeach;

    wp_reset_query();

    ?>

    </div>

    <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="float-right fa-but button button-highlight">Voir tous les produits de la boutique <i class="fa fa-check" aria-hidden="true"></i></a>

    </div>

    <?php endif; ?>

</section>
<?php get_footer() ?>
