<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title itemprop='name'><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('stylesheet_directory'); ?>/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('stylesheet_directory'); ?>/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo('stylesheet_directory'); ?>/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="author" content="Manzell Paula">

    <?php get_template_part("meta-head"); ?>

    <!-- mobile specific meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- blogging specific meta -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!-- CSS -->
    <link href='https://fonts.googleapis.com/css?family=Karla:700,400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/foundation.min.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/buttons.min.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css">

    <?php wp_head(); ?>


</head>

<body itemscope itemtype="http://schema.org/WebSite">

    <div class="off-canvas-wrapper">

        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

            <div class="off-canvas position-left" id="offCanvas" data-off-canvas>

                <!-- Menu -->
                <ul class="vertical menu">
                    <?php

                        echo get_menu_links('header-menu');

                    ?>
                </ul>

            </div>

            <div class="off-canvas-content" data-off-canvas-content>

            <header id="header">

            <div id="head-rs-icon" class="clearfix">

                <div class="row">

                    <div class="float-left">

                        <button class="menu-icon show-for-small-only" type="button" data-open="offCanvas" aria-expanded="false" aria-controls="offCanvas">

                        </button>

                        <p class="phone">Téléphone : +262 692 10 69 43 - Du lundi au samedi, de 9h à 20h</p>

                    </div>

                    <div class="float-right">

                        <a href="https://www.youtube.com/channel/UCSJpvQFWYNnxgnZBKbGA73w/feed" ><i class="fa fa-youtube"></i></a>
                        <a href="https://www.facebook.com/974attitude/" ><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/974attitude" ><i class="fa fa-twitter"></i></a>
                        <a href="<?php bloginfo('url'); ?>/contact" ><i class="fa fa-envelope-o"></i></a>

                    </div>

                </div>

            </div>

        <div class="row" id="identity">

            <?php if(is_home()): ?>

            <h1 class="site-name"><?php echo bloginfo('site_name'); ?> </h1>

            <?php else : ?>

            <div class="site-name"><?php echo bloginfo('site_name'); ?> </div>

            <?php endif; ?>

            <p class="description"><?php echo bloginfo('description'); ?></p>

        </div>

        <nav id="navigation-head">

            <div data-sticky-container>

                <div id="menu-scroll"  data-sticky data-options="anchor: page; marginTop:0;">

                <div id="header-menu" class="row title-bar">

                    <div class="title-bar-left hide-for-small-only">

                            <ul class="menu">

                            <?php

                            global $woocommerce;
                            $number_item = $woocommerce->cart->cart_contents_count;
                            $total_cart = $woocommerce->cart->get_cart_total();
                            echo get_menu_links('header-menu');

                            ?>

                            </ul>

                    </div>

                    <div class="title-bar-right">

                        <a class="button" href="<?php echo $woocommerce->cart->get_cart_url() ?>">

                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>

                            <?php echo $number_item; ?> produit<?php echo ($number_item > 1) ? 's' : ''; ?> - <?php echo $total_cart ?>

                        </a>

                    </div>

                </div>

            </div>

        </div>

    </nav>

</header>

<div id="page">

    <section class="row box-cont">

        <div class="small-12 medium-12 large-12 columns">

            <?php if(!is_home()) { woocommerce_breadcrumb(); } ?>

            <form id="searchform" role="search" method="get" class="woocommerce-product-search" action="<?php echo bloginfo('url'); ?>">

                <div class="input-group">

                <input type="search" id="woocommerce-product-search-field" class="search-field input-group-field" placeholder="Recherche une boisson, un condiment, (Rode un n'affaire !)" value="" name="s" title="Recherche pour&nbsp;:">

                <div class="input-group-button">

                    <button type="submit" href="#" class="button button-highlight"><i class="fa fa-search"></i></button>

                </div>

                </div>

            </form>

        </div>

    </section>
