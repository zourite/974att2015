﻿<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Merci de ne pas lancer cette page directement');

	if ( post_password_required() ) { ?>
		<p class="nocomments">Cet article est protégé par un mot de passe. Entrez ce mot de passe pour lire les commentaires.</p>
	<?php
		return;
	}
?>
<!-- You can start editing here. -->
<?php if ('open' == $post->comment_status) : ?>

<hr>
	<div id="comments">

		<div>

			<div class="cancel-comment-reply">

			<small><?php cancel_comment_reply_link(); ?></small>

		</div>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>Vous devez être  <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">connecté</a> pour publier un commentaire.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="clearfix">
	<?php

    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );

    $args = array (

        'class_submit'      => 'button button-highlight',
        'comment_notes_after' => '',
        'comment_field' =>  '<p class="comment-form-comment"><label class="textarea-label small-2 text-center" for="comment">' . _x( 'Comment', 'noun' ) .
                            '</label><textarea id="comment" name="comment" rows="8" aria-required="true">' .
                            '</textarea></p>',

        'fields' => array(

            'author' =>

    '<div class="comment-form-author">

        <div class="input-group ">

            <span class="input-group-label small-2 prefix">' . __( 'Pseudo ', 'domainreference' ).

                ( $req ? '<span class="required">*</span></span>' : '</span>' ) .

        '

                <input id="author" class="input-group-field" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
    '"' . $aria_req . ' />

        </div>

    </div> ',

            'email' =>

    '<div class="collapse comment-form-email">

        <div class="input-group ">

            <span class="input-group-label small-2 prefix">' . __( 'Email ', 'domainreference' ) .

                ( $req ? '<span class="required">*</span></span>' : '</span>' ) .

        '

            <input id="email" class="input-group-field" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
    '"' . $aria_req . ' />

        </div>

    </div>'


        ));

    comment_form($args);

    ?>
</form>

</div>


<?php endif; // If registration required and not logged in ?>
<?php if ( have_comments() ) : ?>

	<ol class="commentlist">
        <?php wp_list_comments(array( 'style' => 'ol' , 'type' => 'comment', 'callback' => 'format_comment')); ?>
	</ol>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments">Les commentaires sont fermés.</p>

	<?php endif; ?>

<?php endif; ?>
</div>
<?php endif; // if you delete this the sky will fall on your head ?>
