<?php

$img_src = get_stylesheet_directory_uri() . '/img/banniere_974attitude.jpg';
$title = get_bloginfo('site_name');
$description = get_bloginfo('description');
$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if(is_single()) :

    if(has_post_thumbnail($post->ID)) :

            $img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
            $img_src = $img_src[0];

    endif;

    $title = get_the_title();
    $description = strip_tags(get_the_excerpt());
    $link = get_permalink();

endif;

if(is_category()) :

    $categories = get_category(get_query_var('cat'));
    $category_id = $categories->cat_ID;
    $link = get_category_link($category_id);
    $title = $categories->name;

endif;

if(is_tag()) :

    $tags = get_the_tags();
    $tag_id = $tags[0]->term_id;
    $link = get_tag_link($tag_id);
    $title = $tags[0]->name ;

endif;

?>

<?php if(is_product()):

global $post;
$id_post = $post->ID;

$tag_product = get_products_tag($id_post);
$cat_product = get_products_category($id_post);

$keywords = array($cat_product->slug);

if(is_array($tag_product)):

    foreach ($tag_product as $value) :

        $keywords[] = $value[1];

    endforeach;

endif;

$keywords = implode(",",$keywords);

?>

    <meta name="keywords" itemprop="keywords" content="<?php echo $keywords ?>">

<?php endif; ?>

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $title ?>"/>
<meta itemprop="description" content="<?php echo $description ?>"/>
<meta itemprop="image" content="<?php echo $img_src; ?>"/>

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@974attitude">
<meta name="twitter:title" content="<?php echo $title; ?>"/>
<meta name="twitter:description" content="<?php echo $description; ?>"/>
<meta name="twitter:creator" content="@974attitude">
<meta name="twitter:image:src" content="<?php echo $img_src; ?>">

<!-- Open Graph data -->
<?php if (is_single()) : ?>

<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo $link ?>" />
<meta property="og:image" content="<?php echo $img_src; ?>" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:site_name" content="974 Attitude" />

<?php endif; ?>

