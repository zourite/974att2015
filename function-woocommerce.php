<?php


add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );
add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );

function jk_dequeue_styles( $enqueue_styles ) {

    unset( $enqueue_styles['woocommerce-general'] );    // Remove the gloss
    unset( $enqueue_styles['woocommerce-layout'] );     // Remove the layout
    //unset( $enqueue_styles['woocommerce-smallscreen'] );    // Remove the smallscreen optimisation
    return $enqueue_styles;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );

add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_delimiter' );
function jk_change_breadcrumb_delimiter( $defaults ) {
    // Change the breadcrumb delimeter from '/' to '>'
    $defaults['delimiter'] = ' <i class="fa fa-angle-right"></i> ';
    return $defaults;
}

add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
function woo_custom_breadrumb_home_url() {
    return '';
}

add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_home_text' );
function jk_change_breadcrumb_home_text( $defaults ) {
    // Change the breadcrumb home text from 'Home' to 'Appartment'
    $defaults['home'] = 'Tu es içi';
    return $defaults;
}


function custom_override_shipping_fields($fields) {

    unset($fields['shipping_company']);
    unset($fields['shipping_state']);

    $fields['shipping_first_name']['class'] = array('small-6', 'column');
    $fields['shipping_last_name']['class'] = array('small-6', 'column');
    $fields['shipping_city']['class'] = array('small-6', 'column');
    $fields['shipping_postcode']['class'] = array('small-6', 'column');
    $fields['shipping_country']['class'] = array('small-12', 'column');
    $fields['shipping_address_1']['class'] = array('small-12', 'column');
    $fields['shipping_address_2']['class'] = array('small-12', 'column');

    return $fields;

}

function custom_override_checkout_fields( $fields ) {
    // unset($fields['billing']['billing_first_name']);
    // unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_state']);
    unset($fields['billing']['billing_state']);
     //$fields['order']['order_comments']['placeholder'] = 'My new placeholder';
    $fields['billing']['billing_first_name']['class'] = array('small-6', 'column');
    $fields['billing']['billing_last_name']['class'] = array('small-6', 'column');
    $fields['billing']['billing_city']['class'] = array('small-6', 'column');
    $fields['billing']['billing_postcode']['class'] = array('small-6', 'column');
    $fields['billing']['billing_phone']['class'] = array('small-6', 'column');
    $fields['billing']['billing_email']['class'] = array('small-6', 'column');
    $fields['billing']['billing_country']['class'] = array('small-12', 'column');
    $fields['billing']['billing_address_1']['class'] = array('small-12', 'column');
    $fields['billing']['billing_address_2']['class'] = array('small-12', 'column');

    $fields['shipping']['shipping_first_name']['class'] = array('small-6', 'column');
    $fields['shipping']['shipping_last_name']['class'] = array('small-6', 'column');
    $fields['shipping']['shipping_city']['class'] = array('small-6', 'column');
    $fields['shipping']['shipping_postcode']['class'] = array('small-6', 'column');
    $fields['shipping']['shipping_country']['class'] = array('small-12', 'column');
    $fields['shipping']['shipping_address_1']['class'] = array('small-12', 'column');
    $fields['shipping']['shipping_address_2']['class'] = array('small-12', 'column');

    $fields['order']['order_comments']['label'] = 'Des informations complémentaires à me donner ?';

    return $fields;
}

function get_rate_star($product)
{

                $i = $product->get_average_rating();
                $wh_is_fl = strpos($i, '.');

                if($wh_is_fl !== false) :

                    $review = substr($i, 0, $wh_is_fl);

                    $note = $review + 1;

                    while($review > 0) : ?>

                        <i class="fa fa-star"></i>

                    <?php

                    $review--;
                    endwhile;  ?>

                    <i class="fa fa-star-half-o"></i>
            <?php

                else :

                    $note = $i;
                    while ($i > 0) : ?>

                        <i class="fa fa-star"></i>

            <?php
                    $i--;
                    endwhile;

                endif;

                $rest_star = 5 - $note ;

                while ($rest_star > 0) : ?>
                <i class="fa fa-star-o"></i>

            <?php
                   $rest_star--;
                endwhile;

}

function get_products_tag($id_post) {

    $tags = get_the_terms($id_post, 'product_tag');

    if ($tags) :

        foreach ($tags as $key => $value) :

            $products_tags [] = array($value->term_id, $value->name);

        endforeach;

        return $products_tags;

    else :

        return false;

    endif;

}

function get_related_product_for_single(){

    $tags = get_the_tags();
    $terms = array();

    if($tags) :

        foreach($tags as $tag) :

           $terms[] = $tag->slug;

        endforeach;

    endif;


    $args     = array( 'post_type' => 'product', 'orderby' => 'rand' , 'posts_per_page'   => 4,

        'tax_query' => array(

            'relation' => 'OR',

            array(
                    'taxonomy' => 'product_tag',
                    'field'    => 'slug',
                    'terms'    => $terms),

            array(  'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => $terms )
            ));

    $posts = get_posts($args);

    return $posts;
}

function get_products_category($id_post) {

   $category = wp_get_post_terms($id_post,'product_cat' );
   $category = array_shift( $category );

   return $category;

}

function get_tags_in_cat() {

    global $wp_query;
    $posttag = array();

    if(is_product_tag() || is_product_category()) :

        $cat = $wp_query->get_queried_object();

        $args =

            array(

                'post_type' => 'product',
                'tax_query' => array(
                                array(

                                    'taxonomy' => $cat->taxonomy,
                                    'field' => 'id',
                                    'terms' => $cat->term_id,
                                    'include_children' => false
                                )
                            )
                );

        $products = get_posts($args);



    else :

        $products = $wp_query->posts ;

    endif;

    foreach ($products as $product) :

        $tags[] = get_the_terms( $product->ID, 'product_tag' );

    endforeach;

    if ($tags):

        $tags = array_filter($tags);

        foreach($tags as $tag) :

            foreach($tag as $taglist) :

                $current_tag = get_queried_object();
                $current_slug = $current_tag->slug;

                if($current_slug != $taglist->slug) :

                $link = get_term_link($taglist->term_id);

                $posttag[] = array("name" => $taglist->name, "link" => $link);

                endif;

            endforeach;

        endforeach;

    endif;

    if($posttag):

        $posttag = array_map('unserialize', array_unique(array_map('serialize', $posttag)));

    endif;

    return $posttag;

}

function get_woo_search_cat() {

    global $wp_query;

    $list = array();
    $posts = $wp_query->posts;


    foreach ($posts as $post) :

        $cat = get_products_category($post->ID);

        $link = get_term_link($cat->term_id);

        $list[] = array('name' => $cat->name, 'link' => $link );

    endforeach;

        $list = array_map('unserialize', array_unique(array_map('serialize', $list)));

    return $list;

}

function get_sub_cat() {

    global $wp_query;
    $list_cat = array();

    $cat = $wp_query->get_queried_object();
    $cat_id = $cat->term_id;

    $args = array(

       'hierarchical' => 1,

       'show_option_none' => '',

       'hide_empty' => 0,

       'parent' => $cat_id,

       'taxonomy' => 'product_cat'

   );

    $list_subcats = get_categories($args);

    foreach ($list_subcats as $subcats) :

        $link = get_term_link($subcats->term_id);

        $list_cat[] = array("name" => $subcats->name, "link" => $link);

    endforeach;

        $list_cat = array_map('unserialize', array_unique(array_map('serialize', $list_cat)));

    return $list_cat;

}

function benef($order = false) {

    global $woocommerce ;

    $paypal = 3.4;
    $com = 0.25;
    $marge = 25;
    $impots = 11;

    $total_product = $woocommerce->cart->cart_contents_total;

    $total_cart = $woocommerce->cart->total;
    $total_buy = 0;

    if ($order == true) :

        $total_product = 0;
        $total_cart = $order->get_total();

        foreach ( $order->get_items() as $cart_item_key => $cart_item ) :

        $qty = $cart_item['item_meta']['_qty'][0];
        $unit_price = $cart_item['item_meta']['_line_total'][0] / $qty;
        $buy_price = $unit_price * (1 - $marge/100);

        $total_product += $cart_item['item_meta']['_line_total'][0];
        $total_buy += $buy_price * $qty;

        endforeach;

    else :

        foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) :

        $qty = $cart_item['quantity'];
        $buy_price = $cart_item['data']->price * (1 - $marge/100);

        $total_buy += $buy_price * $qty;

        endforeach;

    endif;


    $after_pay = $total_product * (1 - $paypal/100)- 0.25;
    $benef = ($after_pay - $total_buy) * (1 -$impots/100);
    $percent_benef = ($benef * 100) / $total_cart;

    $sold = array($benef, $percent_benef);

    return $sold;
}
