<?php

remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering', 30);
remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating', 5);

remove_action( 'wp_head', 'wp_generator' );

update_option( 'homepage-thumb', 1 );
add_image_size( 'homepage-thumb', 670, 200, true );

update_option( 'single-thumb', 1 );
add_image_size( 'single-thumb', 970, 100, true );

add_theme_support( 'post-thumbnails' );

function register_my_menu() {

  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('footer-menu',__( 'Footer Menu' ));

}

add_action( 'init', 'register_my_menu' );

if ( ! function_exists( 'seobreadcrumbs' ) ) :

    function seobreadcrumbs() {

        $separator = '<i class="fa fa-angle-right"></i>';
        $home = get_bloginfo('url');

        echo '<div class="pad" xmlns:v="http://rdf.data-vocabulary.org/#">';
        global $post;

        echo '<span typeof="v:Breadcrumb">
        <a rel="v:url" property="v:title" href="'.$home.'">Accueil</a>
        </span> ';

        $category = get_the_category();

        if ($category && !is_search() && !is_category() && !is_tag()) :

            if($category[0]->category_parent != 0) :

                 echo $separator . '<span typeof="v:Breadcrumb">

            <a rel="v:url" property="v:title" href="'.get_category_link($category[0]->category_parent).'" >'.get_cat_name( $category[0]->category_parent ).'</a>
            </span>';


            endif;

            echo $separator . '<span typeof="v:Breadcrumb">

            <a rel="v:url" property="v:title" href="'.get_category_link($category[0]->term_id).'" >'.$category[0]->name.'</a>
            </span>';

        endif;

        if (is_single()) :

            echo $separator.' '.get_the_title();

        endif;

        if (is_search()) :

            echo $separator.' Recherche concernant : '.ucfirst(get_search_query());

        endif;

        if (is_category()) :

            echo $separator.' '.single_cat_title('',false);

        endif;

        if (is_tag()) :

            echo $separator.' '.single_tag_title('',false);

        endif;

        echo '</div>';}

endif;

function ribbon() {

    $ribbon = "ribbonPen" ;

    if(in_category(array('6','11')))
    $ribbon = "ribbonV" ;

    if(in_category('77'))
    $ribbon = "ribbonP" ;

    if(in_category('8'))
    $ribbon = "ribbonA" ;

    echo $ribbon;

}

function get_menu_links($menu) {

    $menu_items = wp_get_nav_menu_items ($menu);

    $menu_list = "";

    foreach ($menu_items as $key => $menu_item ) :

        $title = $menu_item->title;
        $url = $menu_item->url;
        $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';

    endforeach;

    return $menu_list;

}

if ( ! function_exists( 'seobreadcrumbs' ) ) :

    function seobreadcrumbs() {

        $separator = '<i class="fa fa-angle-right"></i>';
        $home = get_bloginfo('url');

        echo '<div xmlns:v="http://rdf.data-vocabulary.org/#">';
        global $post;

        echo '<span typeof="v:Breadcrumb">
        <a rel="v:url" property="v:title" href="'.$home.'">Accueil</a>
        </span> ';

        $category = get_the_category();

        if ($category && !is_search() && !is_category() && !is_tag()) :

            if($category[0]->category_parent != 0) :

                 echo $separator . '<span typeof="v:Breadcrumb">

            <a rel="v:url" property="v:title" href="'.get_category_link($category[0]->category_parent).'" >'.get_cat_name( $category[0]->category_parent ).'</a>
            </span>';


            endif;

            echo $separator . '<span typeof="v:Breadcrumb">

            <a rel="v:url" property="v:title" href="'.get_category_link($category[0]->term_id).'" >'.$category[0]->name.'</a>
            </span>';

        endif;

        if (is_single()) :

            echo $separator.' '.get_the_title();

        endif;

        if (is_search()) :

            echo $separator.' Recherche concernant : '.ucfirst(get_search_query());

        endif;

        if (is_category()) :

            echo $separator.' '.single_cat_title('',false);

        endif;

        if (is_tag()) :

            echo $separator.' '.single_tag_title('',false);

        endif;

        echo '</div>';}

endif;

add_filter('comment_reply_link', 'replace_reply_link_class');


function replace_reply_link_class($class){

    $class = str_replace("class='comment-reply-link", "class='button comment-reply-link", $class);
    return $class;

}

function format_comment($comment, $args, $depth) {

    $GLOBALS['comment'] = $comment; ?>

        <li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">

            <div class="comment-intro clearfix">

                <div class="float-right">

                     <span class="fa-stack">

                            <i class="fa fa-calendar-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x calendar">  <?php echo get_comment_date('d') ?></strong>

                        </span>

                        <span class="fa-stack">

                            <i class="fa fa-calendar-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x calendar">  <?php echo get_comment_date('m') ?></strong>

                        </span>

                        <span class="fa-stack">

                            <i class="fa fa-calendar-o blue fa-stack-2x"></i>
                            <strong class="fa-stack-1x blue calendar"><?php echo get_comment_date('y') ?></strong>

                        </span>

                </div>

                <?php printf(__('%s'), get_comment_author_link()) ?>

            </div>

            <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em><br />
            <?php endif; ?>

            <?php comment_text(); ?>

            <div class="reply">
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
<?php }

if ( ! function_exists( 'att2015_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 * Based on paging nav function from Twenty Fourteen
 */

function att2015_paging_nav() {

    global $wp_query;
    $query = $wp_query;

    $big = 999999999; // need an unlikely integer
    $pagination = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total' => $query->max_num_pages,
        'prev_text' => '<i class="fa fa-arrow-left"></i>',
        'next_text' => '<i class="fa fa-arrow-right"></i>',
        'type' => 'list',
        'mid_size' => 9
            ) );

        if($pagination) : ;
    ?>



            <?php echo str_replace( "<ul class='page-numbers'>", '<ul class="menu text-center" role="navigation" aria-label="Pagination">', $pagination); ?>


    <?php

        endif;
}
endif;

// function schedule_get_product_shop() {

//     $product = file_get_contents('https://api.spreadshirt.net/api/v1/shops/1105302/products');
//     file_put_contents(get_template_directory().'/product.xml', $product);

// }

// function get_product_shop() {

//     $xml = simplexml_load_file(get_template_directory().'/product.xml','SimpleXMLElement', LIBXML_NOCDATA);

// print_r($xml);

// }

include(TEMPLATEPATH . '/function-woocommerce.php');
