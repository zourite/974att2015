<?php get_header() ?>

<section class="row box-cont">

    <div class="small-12 columns">

        <?php if(is_category() || is_tag()): ?>

            <h1 class="orange spacing">Les articles suivants traiteront du sujet <em><?php single_cat_title(); ?></em></h1>

            <?php if (category_description()) { echo category_description(); } ?>

        <?php else : ?>

            <h1 class="orange spacing">Les articles suivants datent de <em><?php echo get_query_var('year'); ?></em></h1>

        <?php endif; ?>

        <div class="row small-up-1 medium-up-2" data-equalizer data-equalize-by-row="true">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="column" >

                <article class="card" data-equalizer-watch itemtype="http://schema.org/Article">

                    <div class="ribbon <?php echo ribbon() ?>"></div>

                    <a href="<?php echo the_permalink(); ?>">

                     <?php

                            if (has_post_thumbnail($post->ID)) :

                                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'homepage-thumb' ); ?>

                    <img itemprop="image" class="thumb-box" src="<?php echo $image[0]; ?>" alt="<?php echo the_title() ?>">

                    <?php else : ?>

                    <img itemprop="image" class="thumb-box" src="<?php bloginfo('stylesheet_directory'); ?>/img/banniere_974attitude.png" alt="<?php echo the_title() ?>">

                    <?php endif; ?>

                    </a>

                    <div class="pad">

                    <h3 itemprop="name"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>

                    <p class="post-desc" itemprop="articleBody">

                        <?php echo get_the_excerpt(); ?>

                        <span class="hashtag">

                            <small>

                            <?php if(get_the_tags()) : $tags = get_the_tags(); foreach($tags as $tag) : ?>

                            <a href="<?php echo get_tag_link($tag->term_id);?>">#<?php echo $tag->name ?></a>

                            <?php  endforeach; endif;?>

                            </small>

                        </span>

                    </p>

                    <meta itemprop="datePublished" content="<?php the_time('c'); ?>"/>

                    </div>

                </article>

                <div class="card-meta meta-list text-right">

                    <span class="calendar-i">

                       <small> <i class="fa fa-calendar"></i> <?php the_time('d F Y'); ?> </small>

                    </span>|

                    <span class="read-i">

                       <small> <i class="fa fa-clock-o"></i>Lecture de <?php echo do_shortcode('[rt_reading_time postfix="minutes"]') ?></small>

                    </span>

                </div>

            </div>

        <?php endwhile; endif; ?>

        </div>

    </div>

</section>

<section class="row">

    <nav class="woocommerce-pagination small-12 card columns menu-centered">



        <?php  echo att2015_paging_nav(); ?>

    </nav>

</section>

<?php get_footer(); ?>
