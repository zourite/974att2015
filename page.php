<?php get_header(); ?>
<section class="row box-cont">

    <div class="small-12 medium-12 large-12 columns">

        <article id="content" class="row column" itemtype="http://schema.org/Article">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <?php if(!is_cart()) : ?>

            <?php if(!is_checkout()) : ?>

            <h1 itemprop="name"><?php the_title(); ?></h1>

             <?php if (has_post_thumbnail($post->ID)) :

                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'single-thumb');

                ?>

                <div id="thumb-single">

                    <img itemprop="image" class="thumb raised margin-bot" src="<?php echo $image[0]; ?>" alt="<?php echo the_title() ?>">

                </div>

            <?php endif; ?>

            <?php endif; endif; ?>

            <div itemprop="articleBody">

            <?php the_content(); ?>

            </div>

            <?php endwhile; endif; ?>


        </article>

    </div>

</section>

<?php get_footer(); ?>
